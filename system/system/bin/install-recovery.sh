#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:9f93256a5139b3cc6a215613780c6337e55be3c3; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:a93e586691a140d50bae02da96e03c72104b5a55 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:9f93256a5139b3cc6a215613780c6337e55be3c3 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
